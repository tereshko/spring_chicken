package me.tereshko.chicken.controllers;

import me.tereshko.chicken.Cookies.UserCookies;
import me.tereshko.chicken.gameCore.Chicken;
import me.tereshko.chicken.gameCore.ChickenStatus;
import me.tereshko.chicken.database.ConnectionDB;

import me.tereshko.chicken.gameCore.SaveLoadGame;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
public class Controller {

    Logger LOGGER = Logger.getLogger(Controller.class);

    boolean isExitRequired = false;
    boolean isChickenAlive = true;


    @RequestMapping(value = "/userAction", name = "value", method = RequestMethod.GET)
    public ChickenStatus userAction(@RequestParam(value = "action", defaultValue = "null")
                                            String action, @CookieValue(value = "sessionId", defaultValue = "null") String fooCookie) {

        while (!isExitRequired || isChickenAlive) {
            if (action.equals("exit")) {
                isExitRequired = true;
            } else {
                Chicken chicken = new Chicken();
                SaveLoadGame saveLoadGame = new SaveLoadGame();

                ConnectionDB connectionDB = new ConnectionDB();
                int userID = connectionDB.getUsernameIDFromSession(fooCookie);

                if (!connectionDB.isUserHasSavedGame(userID)) {
                    chicken.loadFromChicken(chicken);
                } else {
                    chicken.loadFromChicken(saveLoadGame.loadGame(userID));
                }

                if (!action.equals("get_status")){
                    isChickenAlive = chicken.chickenTic(action);
                }

                saveLoadGame.saveGame(userID, chicken);
                return new ChickenStatus(chicken);
            }
            if (!isChickenAlive) {
                //TODO: нужно что-то сделать
                LOGGER.info("Chicken dead");
            }
        }
        return null;
    }

    @RequestMapping(value = "/checkCookies", method = RequestMethod.GET)
    public boolean checkCookies(@CookieValue(value = "sessionId", defaultValue = "null") String fooCookie) {

        boolean isCookiesSet = false;

        if (!fooCookie.equals("null")) {
            ConnectionDB connectionDB = new ConnectionDB();
            int userId = -1;
            userId = connectionDB.getUsernameIDFromSession(fooCookie);

            if (userId != -1) {
                isCookiesSet = true;
                LOGGER.info("User cookies is: " + fooCookie);
            }

        } else {
            LOGGER.error("User cookies is NULL");
            isCookiesSet = false;
        }
        return isCookiesSet;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginExistingUser(@RequestParam(value = "login_field", defaultValue = "false") String login_field,
                                    @RequestParam(value = "password_hex", defaultValue = "false") String password_hex,
                                    HttpServletResponse response) {

        ConnectionDB connectionDB = new ConnectionDB();
        String errorState = null;

        int usernameID = -1;
        usernameID = connectionDB.getUsernameID(login_field);

        if (usernameID != -1) {
            boolean isPasswordCorrect = false;

            isPasswordCorrect = connectionDB.comparePass(usernameID, password_hex);

            if (isPasswordCorrect == true) {

                UserCookies userCookies = new UserCookies();
                Cookie cookie;
                cookie = userCookies.getSessionCookie(usernameID);

                response.addCookie(cookie);

                errorState = "user_authorized";

            } else {
                errorState = "password_incorrect";
            }
            //TODO: авторизация юзера
        } else if (usernameID == -1) {
            errorState = "user_not_registered";
            //TODO: отправить код что такого юзера нет
        }

        return errorState;
    }

    @GetMapping(value = "/registration")
    public String registerNewUser(@RequestParam(value = "login_field", defaultValue = "false") String login_field,
                                  @RequestParam(value = "password_field", defaultValue = "false") String password_field,
                                  HttpServletResponse response) {
        ConnectionDB connectionDB = new ConnectionDB();
        String errorState = "user_already_registered";
        int usernameID = -1;
        usernameID = connectionDB.getUsernameID(login_field);

        if (usernameID == -1) {
            // Add new user
            usernameID = connectionDB.addUser(login_field, password_field);

            UserCookies userCookies = new UserCookies();
            Cookie cookie;
            cookie = userCookies.getSessionCookie(usernameID);

            response.addCookie(cookie);

            errorState = "registered_new_user_id:" + usernameID;
        }

        return errorState;
    }
}

