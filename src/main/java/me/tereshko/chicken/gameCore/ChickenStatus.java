package me.tereshko.chicken.gameCore;

public class ChickenStatus {
    private int seedCount;
    private int eggsCount;
    private boolean isAlive;
    private boolean isEggAvailable;
    private int stepCount;

    public ChickenStatus(Chicken chicken) {
        this.seedCount = chicken.getSeedsCount();
        this.eggsCount = chicken.getEggsCount();
        this.isAlive = chicken.getIsChickenAlive();
        this.isEggAvailable = chicken.getIsEggAvailable();
        this.stepCount = chicken.getStepCount();
    }

    public int getSeedCount() {
        return seedCount;
    }

    public int getEggsCount() {
        return eggsCount;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public boolean isEggAvailable() {
        return isEggAvailable;
    }

    public int getStepCount() {
        return stepCount;
    }

}