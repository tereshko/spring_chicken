package me.tereshko.chicken.gameCore;

import me.tereshko.chicken.database.ConnectionDB;
import java.util.List;

public class SaveLoadGame {

    public void saveGame(int userID, Chicken chicken) {

        ChickenStatus chickenStatus = new ChickenStatus(chicken);
        int seedCount = chickenStatus.getSeedCount();
        int eggsCount = chickenStatus.getEggsCount();
        int isAlive = (chickenStatus.isAlive()) ? 1 : 0;
        int isEggAvailable = (chickenStatus.isEggAvailable()) ? 1 : 0;
        int stepCount = chickenStatus.getStepCount();

        ConnectionDB connectionDBForSave = new ConnectionDB();
        connectionDBForSave.saveUserGame(userID, seedCount, eggsCount, isAlive, isEggAvailable, stepCount);
    }

    public Chicken loadGame(int userID) {
        Chicken newChicken = new Chicken();

        ConnectionDB connectionDB = new ConnectionDB();

        List userGameSave;
        userGameSave = connectionDB.loadUserGame(userID);

        if (userGameSave.size()!=0) {
            newChicken.setSeedsCount((int) userGameSave.get(0));
            newChicken.setEggsCount((int) userGameSave.get(1));
            newChicken.setChickenAlive(((int) userGameSave.get(2)) == 1 ? true : false);
            newChicken.setEggAvailable(((int) userGameSave.get(3)) == 1 ? true : false);
            newChicken.setStepCount((int) userGameSave.get(4));
        }

        return newChicken;
    }
}
