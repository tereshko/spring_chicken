package me.tereshko.chicken.gameCore;

public class Chicken {

    private boolean isChickenAlive = true;
    private int seedsCount = 4;
    private boolean isEggAvailable = false;
    private int eggsCount = 0;
    private int stepCount = 0;

    public int getSeedsCount() {
        return seedsCount;
    }

    public int getStepCount() {
        return stepCount;
    }

    public boolean getIsChickenAlive() {
        return isChickenAlive;
    }

    public boolean getIsEggAvailable() {
        return isEggAvailable;
    }

    public int getEggsCount() {
        return eggsCount;
    }

    public void setChickenAlive(boolean chickenAlive) {
        isChickenAlive = chickenAlive;
    }

    public void setSeedsCount(int seedsCount) {
        this.seedsCount = seedsCount;
    }

    public void setEggAvailable(boolean eggAvailable) {
        isEggAvailable = eggAvailable;
    }

    public void setEggsCount(int eggsCount) {
        this.eggsCount = eggsCount;
    }

    public void setStepCount(int stepCount) {
        this.stepCount = stepCount;
    }

    public boolean chickenTic(String userStepSelected) {

        switch (userStepSelected) {
            case "add_seeds":
                seedsCount = seedsCount + 5;
                break;
            case "take_eggs":
                if (isEggAvailable == false) {
                    //TODO: NO EGGS
                } else {
                    //TODO: took 1 eggs
                    eggsCount = eggsCount + 1;
                    isEggAvailable = false;
                }
                break;
            default:
                break;
        }

        if (seedsCount == 0) {
            isChickenAlive = false;
        }

        stepCount = stepCount + 1;

        if (stepCount == 4) {
            isEggAvailable = true;
            stepCount = 0;
        }

        seedsCount = seedsCount - 1;

        if (isChickenAlive == false) {
            //TODO: do something. chicken is dead
        }
        return isChickenAlive;
    }

    public void loadFromChicken(Chicken chicken) {
        this.setEggsCount(chicken.eggsCount);
        this.setEggAvailable(chicken.isEggAvailable);
        this.setEggsCount(chicken.eggsCount);
        this.setChickenAlive(chicken.isChickenAlive);
        this.setSeedsCount(chicken.seedsCount);
        this.setStepCount(chicken.stepCount);
    }
}