package me.tereshko.chicken.gameCore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class PrintStatusInformation {
    public PrintStatusInformation() {
    }
    private static final Logger LOGGER = LoggerFactory.getLogger(PrintStatusInformation.class);

    public void printInfo(Chicken chicken) {
        LOGGER.info("-----------------------------------------------------");
        LOGGER.info("Seeds count: " + chicken.getSeedsCount());
        if (chicken.getSeedsCount() == 0) {
            LOGGER.info("Chicken will dead after next step, please feed");
        }
        LOGGER.info("Eggs count: " + chicken.getEggsCount());
        LOGGER.info("Is chicken available: " + chicken.getIsChickenAlive());
        LOGGER.info("Is egg available: " + chicken.getIsEggAvailable());
        LOGGER.info("Step count: " + chicken.getStepCount());
        LOGGER.info("-----------------------------------------------------");
    }

    public int getUserStepChoice() {
        boolean isAnswerCorrect = false;
        int userStepSelected = 0;

        while (!isAnswerCorrect) {
            LOGGER.info("1. Add 5 seeds");
            LOGGER.info("2. Take egg");
            LOGGER.info("3. Exit");
            LOGGER.info("Make Actions: ");

            Scanner in = new Scanner(System.in);
            userStepSelected = in.nextInt();
            if (userStepSelected > 0 && userStepSelected <= 3) {
                isAnswerCorrect = true;
            } else {
                LOGGER.info("Wrong answer");
            }
        }
        return userStepSelected;
    }
}