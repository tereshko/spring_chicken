package me.tereshko.chicken.gameCore;

public class UserAction {

    private final int userStepSelected;

    public UserAction(int userStepSelected) {
        this.userStepSelected = userStepSelected;
    }

    public int getUserStepSelected() {
        return userStepSelected;
    }
}
