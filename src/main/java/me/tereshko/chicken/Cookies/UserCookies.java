package me.tereshko.chicken.Cookies;

import me.tereshko.chicken.database.ConnectionDB;

import javax.servlet.http.Cookie;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class UserCookies {

    public Cookie getSessionCookie(int usernameID){
        String idSession = generateIdSession();
        Cookie cookie = new Cookie("sessionId", idSession);

        if (idSession != null) {

            cookie.setPath("/");
            cookie.setHttpOnly(true);
            cookie.setMaxAge(7 * 24 * 60 * 60);

            //add cookie to response
            ConnectionDB connectionDB = new ConnectionDB();
            connectionDB.addIDSession(usernameID, idSession);
        }
        return cookie;

    }

    private String generateIdSession() {
        MessageDigest salt = null;
        try {
            salt = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            salt.update(UUID.randomUUID().toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String digest = bytesToHex(salt.digest());
        return digest;
    }

    private String bytesToHex(byte[] digest) {
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}
