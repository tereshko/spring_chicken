package me.tereshko.chicken.database;

import me.tereshko.chicken.PropertieFile.GetPropertieValue;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ConnectionDB {

    Logger LOGGER = Logger.getLogger(ConnectionDB.class);


    GetPropertieValue getPropertieValue = new GetPropertieValue();
    final private String url = getPropertieValue.getDBUrl();
    final private String user = getPropertieValue.getDBLogin();
    final private String password = getPropertieValue.getDBPassword();

    public int getUsernameIDFromSession(String idSession) {
        LOGGER.info("Get UsernameID from Session use sessionid");
        ResultSet rsQuery = null;
        Connection con = null;
        Statement stmt = null;
        int userId = -1;

        String queryForGetUsernameId = "SELECT iduser FROM sessions WHERE idsession = '" + idSession + "'";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        try {
            rsQuery = stmt.executeQuery(queryForGetUsernameId);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            if (rsQuery.next()) {
                try {
                    userId = Integer.parseInt(rsQuery.getString("iduser"));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            rsQuery.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        LOGGER.info("Get UsernameID from Session use sessionid. usernameID is: " + userId);
        return userId;
    }

    public int getUsernameID(String usernameFromWeb) {
        LOGGER.info("Get UsernameID from Session use username from web");
        ResultSet rsQuery = null;
        Connection con = null;
        Statement stmt = null;
        int userId = -1;

        String queryForGetUsernameTable = "SELECT `username` FROM `userdata`;";
        String queryForGetUsernameId = "SELECT iduser FROM userdata WHERE username ='" + usernameFromWeb + "';";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        try {
            rsQuery = stmt.executeQuery(queryForGetUsernameTable);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        while (true) {
            try {
                if (!rsQuery.next()) break;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            String str = null;

            try {
                str = rsQuery.getString("username");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            if (str.equals(usernameFromWeb)) {
                try {
                    rsQuery = stmt.executeQuery(queryForGetUsernameId);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                try {
                    rsQuery.next();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                try {
                    userId = Integer.parseInt(rsQuery.getString("iduser"));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                break;
            }
        }

        try {
            rsQuery.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        LOGGER.info("Get UsernameID from Session use username from web. usernameID is: " + userId);
        return userId;
    }

    public int addUser(String email, String passwordSHA) {
        LOGGER.info("Add user in to DB");

        ResultSet rsQuery = null;
        Connection con = null;
        Statement stmt = null;
        boolean rs = false;
        Integer userID = -1;

        String query = "INSERT INTO userdata (userdata.username, userdata.password) VALUES ('" + email + "', '" + passwordSHA + "');";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        try {
            rs = stmt.execute(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        if (rs != true) {
            String queryForGetID = "SELECT iduser FROM `userdata` WHERE username='" + email + "';";

            try {
                rsQuery = stmt.executeQuery(queryForGetID);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            try {
                if (rsQuery.next()) {
                    try {
                        userID = rsQuery.getInt("iduser");
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            try {
                rsQuery.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }

        try {
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        LOGGER.info("Add user in to DB. User added. usernameID: " + userID);
        return userID;
    }

    public void addIDSession(int userID, String IDSession) {
        LOGGER.info("Add session ID in to the DB");

        Connection con = null;
        Statement stmt = null;

        String queryAddIDSession = "INSERT INTO sessions (sessions.iduser, sessions.idsession) VALUES ('" + userID + "', '" + IDSession + "');";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        try {
            stmt.execute(queryAddIDSession);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
        LOGGER.info("Add session ID in to the DB. Added");
    }

    public boolean comparePass(int userID, String passwordFromWeb) {
        LOGGER.info("Compare password in the DB and from WEB");

        ResultSet rsQuery = null;
        Connection con = null;
        Statement stmt = null;

        String queryForGetPassword = "SELECT password FROM userdata WHERE iduser ='" + userID + "';";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        try {
            rsQuery = stmt.executeQuery(queryForGetPassword);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        String userPasswordFromDB = null;

        try {
            if (rsQuery.next()) {
                try {
                    userPasswordFromDB = rsQuery.getString("password");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            rsQuery.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        boolean isPasswordMatch = userPasswordFromDB.equals(passwordFromWeb);

        LOGGER.info("Compare password in the DB and from WEB. Password is equals: " + isPasswordMatch);

        return isPasswordMatch;
    }

    public void saveUserGame(int userID, int seedCount, int eggsCount, int isAlive, int isEggAvailable, int stepCount) {

        LOGGER.info("Save user game for userId: " + userID);

        Connection con = null;
        Statement stmt = null;

        String queryForSave = "INSERT INTO usergames (" +
                "usergames.iduser, " +
                "usergames.seedcount, " +
                "usergames.eggscount, " +
                "usergames.isalive, " +
                "usergames.iseggavailable, " +
                "usergames.stepcount) " +
                "VALUES ('" + userID + "', '"
                + seedCount + "', '"
                + eggsCount + "', '"
                + isAlive + "', '"
                + isEggAvailable + "', '"
                + stepCount + "');";
        String queryForUpdate = "UPDATE usergames SET " +
                "usergames.seedcount = " + seedCount + "," +
                "usergames.eggscount = " + eggsCount + "," +
                "usergames.isalive = " + isAlive + "," +
                "usergames.iseggavailable = " + isEggAvailable + "," +
                "usergames.stepcount = " + stepCount + "" +
                " WHERE usergames.iduser = " + userID + "";
        boolean isUserHasSavedGame = isUserHasSavedGame(userID);

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        if (isUserHasSavedGame) {
            try {
                stmt.execute(queryForUpdate);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } else {
            try {
                stmt.execute(queryForSave);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        try {
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
        LOGGER.info("User game saved");
    }

    public boolean isUserHasSavedGame(int userID) {
        LOGGER.info("Check that user has saved game, userID: " + userID);
        ResultSet rsQuery = null;
        Connection con = null;
        Statement stmt = null;
        boolean isUserHasSavedGame = false;

        String queryForLoad = "SELECT * FROM usergames WHERE iduser ='" + userID + "';";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        try {
            rsQuery = stmt.executeQuery(queryForLoad);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            if (rsQuery.next()) {
                int userIDFromDB;
                try {
                    userIDFromDB = rsQuery.getInt("iduser");
                    if (userID == userIDFromDB) {
                        isUserHasSavedGame = true;
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                try {
                    rsQuery.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        LOGGER.info("Check that user has saved game, userID: " + userID + ". isGameSaved: " + isUserHasSavedGame);

        return isUserHasSavedGame;
    }

    public List loadUserGame(int userID) {
        LOGGER.info("Load user game for userID: " + userID);

        ResultSet rsQuery = null;
        Connection con = null;
        Statement stmt = null;

        String queryForLoad = "SELECT * FROM usergames WHERE iduser ='" + userID + "';";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        try {
            rsQuery = stmt.executeQuery(queryForLoad);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        List userLoad = new ArrayList();

        try {
            if (rsQuery.next()) {
                int seedcount, eggcount, isalive, iseggavailable, stepcount;
                while (true) {
                    try {
                        seedcount = rsQuery.getInt("seedcount");
                        eggcount = rsQuery.getInt("eggscount");
                        isalive = rsQuery.getInt("isalive");
                        iseggavailable = rsQuery.getInt("iseggavailable");
                        stepcount = rsQuery.getInt("stepcount");
                        userLoad.add(seedcount);
                        userLoad.add(eggcount);
                        userLoad.add(isalive);
                        userLoad.add(iseggavailable);
                        userLoad.add(stepcount);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    break;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            rsQuery.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        try {
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }

        LOGGER.info("Load user game for userID: " + userID + ". Game loaded");

        return userLoad;
    }
}
