package me.tereshko.chicken.PropertieFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetPropertieValue {
    public String getDBUrl() {
        String db_url = readPropertieFromFile("DB_url");
        return db_url;
    }

    public String getDBLogin() {
        String db_user = readPropertieFromFile("DB_user");
        return db_user;
    }

    public String getDBPassword() {
        String db_password = readPropertieFromFile("DB_password");
        return db_password;
    }

    private String readPropertieFromFile(String propertyName) {
        String result = null;
        InputStream inputStream = null;

        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            result = prop.getProperty(propertyName);

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try{
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
            }

        }
        return result;
    }
}
