'use strict';
let isCookiesSet = checkCookies();

fetch_data("get_status");

document.getElementById('add_seeds').onclick = function(){
  fetch_data('add_seeds');;
}
document.getElementById('take_eggs').onclick = function(){
  fetch_data('take_eggs');
}
document.getElementById('exit').onclick = function(){
  fetch_data('exit');
}

async function fetch_data(action){
  let isCookiesSet = checkCookies();
  
  if(isCookiesSet){
    let url = '/userAction?action='+action;
    let response = await fetch(url);
    let json = await response.json();
  
    document.getElementById('alive').innerText = json.alive;
    document.getElementById('seedCount').innerText = json.seedCount;
    document.getElementById('eggsCount').innerText = json.eggsCount;
    document.getElementById('eggAvailable').innerText = json.eggAvailable;
    document.getElementById('stepCount').innerText = json.stepCount;

  } else{
    alert('no cookies');
  }

}

async function checkCookies(){
  let url = '/checkCookies';
  let response = await fetch(url);
  let json = await response.json();
  if(json==false){
    json==false;
    document.location.href = '/registration/index.html';
  } else{
    json==true;
  }
  return json;
}
