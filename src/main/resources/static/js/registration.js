'use strict';
let isCookiesSet = checkCookies();

document.getElementById('register_button').onclick = function(){
    login_form_action();
  }
  
  async function login_form_action(){
    let value_in_login_field = document.getElementById('username').value;
    let value_in_password_field = document.getElementById('password').value;
    if(value_in_login_field=="" || value_in_password_field==""){
      if(value_in_login_field==""){
        document.getElementById('username').placeholder = "*required";
      }
      if(value_in_password_field==""){
        document.getElementById('password').placeholder = "*required";
      }
    } else{
        const digestHex = await sha256(value_in_password_field);
        let url = '/registration?login_field='+value_in_login_field+'&password_field='+digestHex;
        let response = await fetch(url);
        let json = await response.text();
        let isAuthorized = json.includes("registered_new_user_id:");
        if(isAuthorized){
          console.log('user_registered');
          document.location.href = '/';
        } else{
          console.log('user_already_registered');
          document.getElementById('error_message').style.visibility = "visible";
        }
    }
  }

  async function sha256(password) {
    const msgUint8 = new TextEncoder().encode(password);                           // encode as (utf-8) Uint8Array
    const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the password
    const hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
    const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
    return hashHex;
  }
  
  async function checkCookies(){
    let url = '/checkCookies';
    let response = await fetch(url);
    let json = await response.json();
    if(json==true){
      document.location.href = '/';
    }
    return json;
  }